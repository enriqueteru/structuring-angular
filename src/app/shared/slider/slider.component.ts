import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
})
export class SliderComponent implements OnInit {
  @Input() title?: string;
  @Input() buttonText?: string;
  @Input() buttonLink?: string;
  @Input() colorBackground?: string;

  constructor() {}

  ngOnInit(): void {}
}
