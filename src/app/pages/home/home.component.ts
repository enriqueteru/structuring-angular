import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  public sliderHome: string = 'Bienvenido/a'
  public buttonText: string = 'See proyects'
  public homeButtonLink: string = ''
  public color:string = 'home'

  constructor() {

}
  ngOnInit(): void {
  }

}
