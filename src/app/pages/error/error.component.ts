import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
  public errorText:string = "Error!!!"
  public buttonText:string = "Back to home"
  public color: string = "error"
  constructor() {

  }

  ngOnInit(): void {
  }

}
