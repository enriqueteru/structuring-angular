import { Component, OnInit } from '@angular/core';
import { Proyect } from './models/Proyect.model';
@Component({
  selector: 'app-proyects',
  templateUrl: './proyects.component.html',
  styleUrls: ['./proyects.component.scss'],
})
export class ProyectsComponent implements OnInit {
  public proyects: Array<Proyect>;
  public fav?: Proyect;

  constructor() {
    this.proyects = [
      new Proyect(
        'Headon',
        'marketing',
        'https://enriqueteruel.com/wp-content/uploads/2021/04/headon-asphyxia-portada-scaled.jpg',
        2018
      ),

      new Proyect(
        'Evohe',
        'Branding',
        'https://enriqueteruel.com/wp-content/uploads/2019/11/Untitled-1-05-1.jpg',
        2019
      ),

      new Proyect(
        'Gestiona 21',
        'Packaging',
        'https://enriqueteruel.com/wp-content/uploads/2019/10/g21_proy_Mesa-de-trabajo-1-copia-2.png',
        2020
      ),

      new Proyect(
        'photogo',
        'Web',
        'https://enriqueteruel.com/wp-content/uploads/2019/10/quierola_web__Mesa-de-trabajo-1-copia.png',
        2017
      ),
    ];
  }

  ngOnInit(): void {

  }
showFav(event: any){
  this.fav = event.fav;

}
}
