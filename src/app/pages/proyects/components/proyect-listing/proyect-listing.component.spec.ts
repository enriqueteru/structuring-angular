import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectListingComponent } from './proyect-listing.component';

describe('ProyectListingComponent', () => {
  let component: ProyectListingComponent;
  let fixture: ComponentFixture<ProyectListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProyectListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
