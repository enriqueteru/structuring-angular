import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Proyect } from '../../models/Proyect.model';

@Component({
  selector: 'app-proyect-listing',
  templateUrl: './proyect-listing.component.html',
  styleUrls: ['./proyect-listing.component.scss']
})
export class ProyectListingComponent implements OnInit {

  @Input()
  proyectItem?: Proyect;
  @Input() posicion?: number;
  @Output() favSelection = new EventEmitter();
  constructor() { }

  ngOnInit(): void {

  }

deleteProyect(){}

editProyect(){}


favProyect(event: any, proyectItem: any){
 this.favSelection.emit({fav: proyectItem})
}

}
