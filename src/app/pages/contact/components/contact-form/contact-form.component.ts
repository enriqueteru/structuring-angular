import { Component, OnInit } from '@angular/core';
import { Client } from '../../interfaces/client';
@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {

public data: Client = {};

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(){
    console.log(this.data)
  }

}
