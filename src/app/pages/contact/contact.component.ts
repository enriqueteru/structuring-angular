import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {


  public contactTitleText?: string
  public contactButtonText?:string
  public color?:string = "contact"
  constructor() {

 this.contactTitleText = "Contact us"
 this.contactButtonText = "Send Email"

  }

  ngOnInit(): void {
  }

}
