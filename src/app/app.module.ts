import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeContainerComponent } from './pages/home/components/home-container/home-container.component';
import { HomeComponent } from './pages/home/home.component';
import { ErrorComponent } from './pages/error/error.component';
import { ContactComponent } from './pages/contact/contact.component';
import { AboutComponent } from './pages/about/about.component';
import { ProyectsComponent } from './pages/proyects/proyects.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SliderComponent } from './shared/slider/slider.component';
import { ButtonComponent } from './shared/button/button.component';
import { ProyectListingComponent } from './pages/proyects/components/proyect-listing/proyect-listing.component';
import { ContactFormComponent } from './pages/contact/components/contact-form/contact-form.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeContainerComponent,
    HomeComponent,
    ErrorComponent,
    ContactComponent,
    AboutComponent,
    ProyectsComponent,
    HeaderComponent,
    FooterComponent,
    SliderComponent,
    ButtonComponent,
    ProyectListingComponent,
    ContactFormComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
